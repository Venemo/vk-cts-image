#!/bin/bash

set -ex

DEQP_RUNNER_VERSION="mesa-ci-2019-12-17"

git clone \
    --depth 1 \
    https://gitlab.freedesktop.org/mesa/parallel-deqp-runner.git \
    -b $DEQP_RUNNER_VERSION \
    /parallel-deqp-runner

pushd /parallel-deqp-runner
meson build/
ninja -C build install
cp build/hang-detection /usr/local/bin/
rm -rf /parallel-deqp-runner
popd
