#!/bin/bash

set -ex

git clone https://gitlab.freedesktop.org/pendingchaos/shader-db.git /shader-db
pushd /shader-db
git checkout fossil_stuff
cp fossil_replay.sh /usr/local/bin
popd
rm -rf /shader-db
