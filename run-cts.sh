#!/bin/bash

TEST_IMAGE="registry.freedesktop.org/hakzsam/vk-cts-image:2020-04-17"

result_dir=`pwd`/results
compiler="aco"
gpu_families=(pitcairn
              fiji
              polaris10
              vega10
              navi10)

while getopts "b:c:d:g:j:r:" OPTION; do
    case $OPTION in
    b)
        mesa_branch=$OPTARG
        ;;
    c)
        compiler=$OPTARG
        ;;
    d)
        result_dir=$OPTARG
        ;;
    g)
        gpu_family=$OPTARG
        ;;
    j)
        num_threads=$OPTARG
        ;;
    r)
        mesa_remote=$OPTARG
        ;;
    *)
        if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ]; then
            echo "Non-option argument: '-${OPTARG}'" >&2
        fi
        exit 1
        ;;
    esac
done

if [ -z $mesa_remote ] || [ -z $mesa_branch ] || [ -z $gpu_family ]; then
    echo "$0 -r <mesa_remote> -b <mesa_branch> -f <gpu_family> [-j <deqp_parallel>] [-d <result_dir>}"
    exit 1
fi

found=0
for g in "${gpu_families[@]}"
do
    if [ "$g" == "$gpu_family" ] ; then
        found=1
    fi
done

if [ $found -eq 0 ]; then
    echo "Unsupported GPU family option (accepted values: ${gpu_families[@]})"
    exit 1
fi

if [ "$compiler" != "aco" ] && [ "$compiler" != "llvm" ]; then
    echo "Invalid compiler option (accepted values are: aco, llvm)"
    exit 1
fi

# Default list of skipped/expected tests.
deqp_skips="deqp-radv-default-skips.txt"
deqp_expected_failures=""

# Select the list of expected failures (only supported for ACO).
if [ "$compiler" == "aco" ]; then
    deqp_expected_fails="deqp-radv-$gpu_family-aco-fails.txt"
fi

# Enable ACO via RADV_PERFTEST if set.
radv_perftest="aco"
if [ "$compiler" == "llvm" ]; then
    radv_perftest=""
fi

set -ex

mkdir -p $result_dir

# Pull the latest image.
docker pull $TEST_IMAGE

# Build a specific Mesa remote/branch and run CTS with deqp-runner.
docker run \
    --device /dev/dri/renderD128 \
    --mount src=`pwd`/testing,target=/mnt/testing,type=bind \
    --mount src=$result_dir,target=/mnt/results,type=bind \
    --env MESA_REMOTE=$mesa_remote \
    --env MESA_BRANCH=$mesa_branch \
    --env NUM_THREADS=$num_threads \
    --env DEQP_SKIPS=$deqp_skips \
    --env DEQP_EXPECTED_FAILS=$deqp_expected_fails \
    --env RADV_PERFTEST=$radv_perftest \
    --env-file env \
    -it $TEST_IMAGE \
    bin/bash /mnt/testing/run-cts.sh
