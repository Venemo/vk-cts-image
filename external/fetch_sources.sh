#!/bin/bash

set +ex

git submodule update --init

cd external/radv_fossils
git lfs pull
