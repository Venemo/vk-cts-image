#!/bin/bash

TEST_IMAGE="registry.freedesktop.org/hakzsam/vk-cts-image:2020-04-17"

result_dir=`pwd`/results
compiler="aco"

while getopts "b:d:j:r:" OPTION; do
    case $OPTION in
    b)
        mesa_branch=$OPTARG
        ;;
    c)
        compiler=$OPTARG
        ;;
    d)
        result_dir=$OPTARG
        ;;
    j)
        num_threads=$OPTARG
        ;;
    r)
        mesa_remote=$OPTARG
        ;;
    *)
        echo "Incorrect options provided"
        exit 1
        ;;
    esac
done

if [ -z $mesa_remote ] || [ -z $mesa_branch ]; then
    echo "$0 -r <mesa_remote> -b <mesa_branch> [-j <fossilize_parallel>] [-d <result_dir>}"
    exit 1
fi

if [ "$compiler" != "aco" ] && [ "$compiler" != "llvm" ]; then
    echo "Invalid compiler option (accepted values are: aco, llvm)"
    exit 1
fi

# Enable ACO via RADV_PERFTEST if set.
radv_perftest="aco"
if [ "$compiler" == "llvm" ]; then
    radv_perftest=""
fi

set -ex

mkdir -p $result_dir

# Pull the latest image.
docker pull $TEST_IMAGE

# Build a specific Mesa remote/branch and run CTS with deqp-runner.
docker run \
    --device /dev/dri/renderD128 \
    --mount src=`pwd`/testing,target=/mnt/testing,type=bind \
    --mount src=`pwd`/external/radv_fossils,target=/mnt/radv_fossils,readonly,type=bind \
    --mount src=$result_dir,target=/mnt/results,type=bind \
    --env MESA_REMOTE=$mesa_remote \
    --env MESA_BRANCH=$mesa_branch \
    --env NUM_THREADS=$num_threads \
    --env RADV_PERFTEST=$radv_perftest \
    --env-file env \
    -it $TEST_IMAGE \
    bin/bash /mnt/testing/run-fossilize.sh
