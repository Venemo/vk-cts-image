#!/bin/bash

set -ex

git clone --depth 1 $MESA_REMOTE -b $MESA_BRANCH /mesa
pushd /mesa

.gitlab-ci/meson-build.sh
.gitlab-ci/prepare-artifacts.sh

set +e

export DEQP_PARALLEL=$NUM_THREADS

install/deqp-runner.sh

if [ $? -ne 0 ]; then
    # Save the list of unexpected failures.
    cp /mesa/results/cts-runner-unexpected-results.txt /mnt/results
fi

popd
