#!/bin/bash

set -ex

#git clone https://gitlab.freedesktop.org/pendingchaos/shader-db.git /shader-db
#pushd /shader-db
#git checkout fossil_stuff
#popd

git clone --depth 1 $MESA_REMOTE -b $MESA_BRANCH /mesa
pushd /mesa
.gitlab-ci/meson-build.sh
popd

export LD_LIBRARY_PATH=/mesa/install/lib/:/usr/local/lib
export VK_ICD_FILENAMES=/mesa/install/share/vulkan/icd.d/"$VK_DRIVER"_icd.x86_64.json

if [ -n "$NUM_THREADS" ]; then
    FOSSILIZE_PARALLEL="--num-threads $NUM_THREADS"
fi

FOSSILIZE_OUTPUT="/mnt/results/fossilize-output.csv"

/usr/local/bin/fossil_replay.sh /mnt/radv_fossils \
    $FOSSILIZE_OUTPUT $FOSSILIZE_PARALLEL
